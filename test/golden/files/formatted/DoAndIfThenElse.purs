module DoAndIfThenElse where

foo = do if true then 0 else 1

bar = do
  if true
    then 0
    else 1
